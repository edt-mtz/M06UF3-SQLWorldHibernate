package com.mtzmanuel.hibernate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Country implements Serializable{

	private static final long serialVersionUID = 4947071892368161537L;
	
	private String countryId;  
	private String countryName;
	private Set<City>  cities =new HashSet<City>();

	public Country() {
	}

	public Country(String id, String name) {
		this.countryId=id;  
		this.countryName=name;

	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String id) {
		this.countryId = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String name) {
		this.countryName = name;
	}

	public Set<City> getCities() {
		return this.cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		return true;
	}

	public boolean addCity(City c) {
		return cities.add(c);
	}

	public boolean removeCity(City c) {
			return cities.remove(c.getCityName());
	}

}  