package com.mtzmanuel.hibernate;

import java.io.Serializable;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class City implements Serializable{

	private static final long serialVersionUcountry = -5962544668771569349L;
	
	private Country countryId;
	private String cityName;

	public City() {
	}
	
	public City(Country country, String cityName) {
		this.countryId=country;  
		this.cityName=cityName; 
	}

	public Country getCountryId() {
		return countryId;
	}

	public void setCountryId(Country pais) {
		this.countryId = pais;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (cityName == null) {
			if (other.cityName != null)
				return false;
		} else if (!cityName.equals(other.cityName))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		return true;
	}
 
}  