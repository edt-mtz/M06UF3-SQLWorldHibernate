package com.mtzmanuel.hibernate;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.xml.xpath.XPathExpressionException;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.w3c.dom.Document;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Main {

    public static Scanner scan = new Scanner(System.in);
    public static String menuItem;

    static Query query;
    static List list;

    private static final String PATH_TO_XML_FILE = "data/world.xml";

    public static void menuPostgres() {
        System.out.println("\n**********************************************");
        System.out.println("*            PostgreSQL Hibernate            *");
        System.out.println("**********************************************");
        System.out.println("[1] Insert data from XML");
        System.out.println("[2] Insert a new country");
        System.out.println("[3] Insert a new city");
        System.out.println("**********************************************");
        System.out.println("[4] Remove a country");
        System.out.println("[5] Remove a city");
        System.out.println("**********************************************");
        System.out.println("[6] Update a country");
        System.out.println("[7] Update a city");
        System.out.println("**********************************************");
        System.out.println("[8] Show all");
        System.out.println("**********************************************");
        System.out.println("[q] Exit");
        System.out.println("**********************************************\n");
        System.out.print("Select: ");
    }

    public static void showMenu() {
        System.out.println("[m] Show main menu");

        System.out.print("Select: ");
    }

    public static void showBye() {
        System.out.println("\nbye");
    }

    public static String toInitCap(String param) {
        if (param != null && param.length() > 0) {
            char[] charArray = param.toCharArray();
            charArray[0] = Character.toUpperCase(charArray[0]);
            // Set capital letter to first position
            return new String(charArray);
            // Return desired output
        } else {
            return "";
        }
    }

    public static void main(String[] args) throws XPathExpressionException, SQLException {

        Document doc;

        Configuration cfg = new Configuration();
        cfg.configure("hibernate.cfg.xml");

        // aquí es donde peta si falla conexión con Postgres
        //creating session factory object
        SessionFactory factory = cfg.buildSessionFactory();

        //creating session object
        Session session = factory.openSession();

        menuPostgres();
        do {
            menuItem = scan.nextLine();

            switch (menuItem) {

                case "1":
                    //creating transaction object
                    Transaction t = session.beginTransaction();

                    doc = World.getDocument(PATH_TO_XML_FILE);

                    // carreguem el document amb DOM perquè la majoria ho heu fet així
                    // es una mostra de com es pot fer eficient amb aquesta lliberia
                    // sinò, fer-lo amb SAX hagués estat una bona opció
                    World.insertCountriesAndCitiesFromXML(doc, session);

                    t.commit();

                    showMenu();
                    break;
                case "2":

                    t = session.beginTransaction();
                    System.out.print("Type a country name: ");
                    String cnt = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a country id name: ");
                    String cntId = toInitCap(scan.nextLine().toLowerCase());


                    Country insertPais = new Country(cntId, cnt);

                    try {
                        session.persist(insertPais);
                        t.commit();
                    } catch (ConstraintViolationException sql) {
                        System.err.println("Constraint Violation 'Country'");
                    } catch (NonUniqueObjectException nue) {
                        System.err.println("ID not unique 'Id'");
                    }

                    showMenu();
                    break;
                case "3":
                    t = session.beginTransaction();
                    System.out.print("Type a city name: ");
                    String ct = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a country name: ");
                    cnt = toInitCap(scan.nextLine().toLowerCase());

                    query = session.createQuery("from Country where initcap(countryName) = :searchKey ");
                    query.setParameter("searchKey", cnt);

                    try {
                        session.persist(new City((Country) query.list().get(0), ct));
                        t.commit();
                    } catch (ConstraintViolationException sql) {
                        System.err.println("Constraint Violation");
                    } catch (NonUniqueObjectException nue) {
                        System.err.println("Already Exists");
                    }

                    showMenu();
                    break;
                case "4":
                    t = session.beginTransaction();
                    System.out.print("Type a country name: ");
                    cnt = toInitCap(scan.nextLine().toLowerCase());

                    query = session.createQuery("from Country where initcap(countryName) = :searchKey ");
                    query.setParameter("searchKey", cnt);

                    session.delete(query.list().get(0));
                    t.commit();

                    showMenu();
                    break;

                case "5":
                    t = session.beginTransaction();
                    System.out.print("Type a city name: ");
                    ct = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a country name: ");
                    cnt = toInitCap(scan.nextLine().toLowerCase());

                    query = session.createQuery("from Country where initcap(countryName) = :searchKey ");
                    query.setParameter("searchKey", cnt);

                    Country tmp = (Country) query.list().get(0);
                    City deleteCity = new City(tmp, ct);
                    session.delete(deleteCity);
                    t.commit();

                    showMenu();
                    break;

                case "6":
                    t = session.beginTransaction();
                    System.out.print("Type a country name: ");
                    cnt = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a new country name: ");
                    String newCnt = toInitCap(scan.nextLine().toLowerCase());

                    query = session.createQuery("update Country set countryName = :searchKey where " +
                            "initcap(countryName) = :searchKey2");
                    query.setParameter("searchKey", newCnt);
                    query.setParameter("searchKey2", cnt);

                    int result = query.executeUpdate();

                    System.out.println(result);

                    t.commit();
                    showMenu();
                    break;
                case "7":

                    System.out.print("Type a city name: ");
                    ct = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a its country name: ");
                    cnt = toInitCap(scan.nextLine().toLowerCase());

                    System.out.print("Type a new city name: ");
                    String newCt = toInitCap(scan.nextLine().toLowerCase());

                    query = session.createQuery("from Country where initcap(countryName) = :searchKey ");
                    query.setParameter("searchKey", cnt);

                    try {
                        tmp = (Country) query.list().get(0);
                        String cId = tmp.getCountryId();

                        t = session.beginTransaction();

                        query = session.createQuery("update City set cityName = :searchKey where " +
                                "upper(countryid) = :searchKey2 and initcap(cityName) = :searchKey3 ");
                        query.setParameter("searchKey", newCt);
                        query.setParameter("searchKey2", cId);
                        query.setParameter("searchKey3", ct);

                        result = query.executeUpdate();

                        System.out.println(result);

                        t.commit();
                    } catch (IndexOutOfBoundsException iou) {
                        System.err.println("It doesn't exist");
                    }

                    showMenu();
                    break;
                case "8":

                    query = session.createQuery("from Country");
                    List <Country> listCountry = query.list();

                    for (Country country : listCountry) {
                        Set<City> cities = country.getCities();
                        for (City city : cities) {
                            System.out.println(city.getCityName() + " " + country.getCountryName() + " " + country.getCountryId());
                        }
                    }


                    showMenu();
                    break;
                case "m":
                    menuPostgres();
                    break;
                case "q":
                    showBye();
                    break;
                default:
                    showMenu();
                    break;
            }

        } while (!menuItem.equals("q"));

        scan.close();

        session.close();
        factory.close();

    }

}
