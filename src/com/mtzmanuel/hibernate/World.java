package com.mtzmanuel.hibernate;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.hibernate.Session;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class World {

	public static Document getDocument(String path) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc = null;

		try {

			builder = factory.newDocumentBuilder();
			doc = builder.parse(path);

		} catch (SAXException | IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		return doc;
	}

	protected static void insertCountriesAndCitiesFromXML(Document doc,Session sess) throws XPathExpressionException, SQLException {

		int  totalCities=0;

		// Create XPathFactory object
		XPathFactory xpathFactory = XPathFactory.newInstance();
		// Create XPath object
		XPath xpath = xpathFactory.newXPath();

		XPathExpression exprGetCountry = xpath.compile("/world/country");
		XPathExpression exprGetCitiesFromCountry = xpath.compile("//city");

		// lista de nodos recuperados
		NodeList countryNodes = (NodeList) exprGetCountry.evaluate(doc,XPathConstants.NODESET);

		final int  countryNodesLength = countryNodes.getLength();

		for (int i = 0; i < countryNodesLength; i++) {

			Node currentCountryNode=countryNodes.item(i).cloneNode(true);

			Country paisActual =new Country(currentCountryNode.getAttributes().getNamedItem("car_code").getNodeValue(), 
					currentCountryNode.getChildNodes().item(1).getTextContent());

			System.out.println(paisActual.getCountryId()+" "+paisActual.getCountryName());
			
			HashSet<City> hs=new HashSet<City>();

			NodeList cityNodes = (NodeList) exprGetCitiesFromCountry.evaluate(currentCountryNode,XPathConstants.NODESET);

			HashSet<String> hsCities=new HashSet<String>();

			for (int j = 0; j < cityNodes.getLength(); j++) {

				Node currentCityNode=cityNodes.item(j);

				//TODO observamos que name está siempre en la misma posición, pero sería más correcto asegurarnos que es <name>
				String nomCiutat = currentCityNode.getFirstChild().getNextSibling().getTextContent();

				if (hsCities.add(nomCiutat)){

					System.out.println("\t"+j+" "+nomCiutat);
					
					hs.add(new City(paisActual,nomCiutat));

					totalCities++;
				}

			}
			paisActual.setCities(hs);

			sess.persist(paisActual);
			
		} 

		System.out.println(	"totalCities : "+	totalCities);

	}
 
}



 